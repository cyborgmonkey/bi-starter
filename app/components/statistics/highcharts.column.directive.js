(function () {
    'use strict';

    angular
        .module('angularVoteApp')
        .directive('highchartsColumn', highchartsColumn);

    /** @ngInject */
    function highchartsColumn($rootScope) {
        var directive = {
            restrict: 'A',
            scope: {
                columnChartData: "="
            },
            controller: HighchartsColumnController,
            controllerAs: 'vm',
            bindToController: true,
            link: function (scope, element, attrs, vm) {

                var options = {
                    chart: {
                        type: 'column',
                        options3d: {
                            enabled: true,
                            alpha: 15,
                            beta: 15,
                            depth: 50,
                            viewDistance: 25
                        }
                    },
                    title: {
                        text: 'Column chart visualization of votes'
                    },
                    xAxis: {
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Vote number'
                        }
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{name: 'Votes on option'}]
                };

                element.highcharts(options);

                function render() {
                    var chart = element.highcharts();
                    chart.xAxis[0].setCategories(vm.columnChartData.categories, true, true);
                    chart.series[0].setData(vm.columnChartData.data);
                }

                render();

                scope.$on("render", render);
            }
        };

        return directive;

        /** @ngInject */
        function HighchartsColumnController($mdDialog, VoteService, OptionService) {
            var vm = this;

        }
    }

})();
