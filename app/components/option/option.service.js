(function() {
  'use strict';

  angular
    .module('angularVoteApp')
    .factory('OptionService', OptionService);

  /** @ngInject */
  function OptionService($firebaseArray, FirebaseDataService) {
    var options = null;

    var service = {
      Option: Option,
      addOptionsToQuestion: addOptionsToQuestion,
      getOptionsToQuestion: getOptionsToQuestion
    };

    init();

    return service;

    function Option(){
      this.label = '';
    }

    function init(){
      options = $firebaseArray(FirebaseDataService.options);
    }

    function addOptionsToQuestion(questionId, options){
      _.forEach(options, function (option) {
        FirebaseDataService.options.child(questionId).push(option);
      });
    }

    function getOptionsToQuestion(question){
      return $firebaseArray(FirebaseDataService.options.child(question.$id));
    }

  }

})();
