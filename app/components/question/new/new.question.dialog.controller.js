(function() {
  'use strict';

  angular
    .module('angularVoteApp')
    .controller('NewQuestionDialogController', NewQuestionDialogController);

  /** @ngInject */
  function NewQuestionDialogController($mdDialog, QuestionService, OptionService, NotificationService) {
    var vm = this;
    vm.question = new QuestionService.Question();
    vm.options = [];

    activate();

    vm.hide = function() {
      $mdDialog.hide();
    };
    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.addNewOption = function () {
      vm.options.push(new OptionService.Option());
    };

    vm.create = function (){
      QuestionService.addQuestion(vm.question).then(function (question) {
        var id = question.key();
        OptionService.addOptionsToQuestion(id,vm.options);
        NotificationService.showNotification("Question created successfully.");
        vm.hide();
      });
    };

    function activate(){
      vm.options.push(new OptionService.Option());
      vm.options.push(new OptionService.Option());
    }
  }
})();
