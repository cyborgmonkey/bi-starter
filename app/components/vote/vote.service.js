(function() {
  'use strict';

  angular
    .module('angularVoteApp')
    .factory('VoteService', VoteService);

  /** @ngInject */
  function VoteService($firebaseArray, FirebaseDataService) {
    var votes = null;

    var service = {
      Vote: Vote,
      addVoteToQuestion: addVoteToQuestion,
      getVotesToQuestion: getVotesToQuestion,
      getVotesToQuestionId: getVotesToQuestionId
    };

    init();

    return service;

    function init(){
      votes = $firebaseArray(FirebaseDataService.votes);
    }

    function Vote(){
      this.option = '';
      this.createdAt = null;
    }

    function addVoteToQuestion(question, option){
      var vote = new Vote();
      vote.createdAt = (new Date()).getTime();
      vote.option = option;

      return FirebaseDataService.votes.child(question.$id).push(vote);
    }

    function getVotesToQuestion(question){
      return $firebaseArray(FirebaseDataService.votes.child(question.$id));
    }

    function getVotesToQuestionId(id){
      return $firebaseArray(FirebaseDataService.votes.child(id));
    }

  }

})();
